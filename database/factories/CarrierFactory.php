<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Carrier;
use Faker\Generator as Faker;

$factory->define(Carrier::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->phoneNumber,
        'current_lat' => (mt_rand(0, 1.0 * pow(10, 7)) / pow(10, 7)) + 30.0,
        'current_lon' => (mt_rand(0, 1.0 * pow(10, 7)) / pow(10, 7)) + 31.0,
    ];
});
