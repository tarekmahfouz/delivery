<?php

namespace App\Http\Controllers\API;

use App\Models\Carrier;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Resources\Carrier as CarrierResource;

class PublicController extends InitController
{
    // Get Closest Carriers To The Selected Market
    public function nearby(Request $request)
    {
        $code = 200;
        $message = 'done';
        $items = [];
        try {
            $page = $request->page;
            $perPage = $request->perPage;
            $baseUrl = route('nearby.carriers', [
                'market_id' => $request->market_id,
                'perPage' => $perPage
            ]);

            $conditions = ['id' => $request->market_id];
            $market = $this->serviceObj->find('Market', $conditions);
            if(!$market) {
                throw new \Exception('not found!', Response::HTTP_NOT_FOUND);
            }
            $list = Carrier::orderByDistance($market->lat, $market->lon);
            $collection = CarrierResource::collection($list);

            $items = paginate($collection, $perPage, $page, $baseUrl);

        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $items);
    }

}
