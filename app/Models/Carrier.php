<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Carrier extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    protected $hidden = ['created_at','updated_at','deleted_at'];
    

    public static function orderByDistance($lat, $lon)
    {
        $results = DB::select(DB::raw("SELECT
            *,
            (
                3959 * ACOS(
                    COS(RADIANS($lat)) * COS(RADIANS(current_lat)) * COS(
                        RADIANS(current_lon) - RADIANS($lon)
                    ) + SIN(RADIANS($lat)) * SIN(RADIANS(current_lat))
                )
            ) AS DISTANCE
            FROM
                carriers
            ORDER BY
                DISTANCE") 
        );
        return $results;
    }
}
