<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends ResponseShape
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'phone' => 'required|unique:users,phone',
                        'email' => 'required|email|unique:users,email',
                        'name' => 'required|min:2',
                        'password' => 'required|min:6|confirmed',
                        'image' => 'image',
                    ];
                }
            case 'PUT': {
                    if (request()->wantsJson() && !request()->ajax()) {
                        return [
                            'name' => 'nullable|min:2',
                            'phone' => 'required|unique:users,phone,' . $this->user()->id,
                            'email'    => 'nullable|email|unique:users,email,' . $this->user()->id,
                            'password' => 'nullable|min:6|confirmed',
                            'image' => 'nullable|image',
                        ];
                    } else {
                        return [
                            'name' => 'nullable|min:2',
                            'phone' => 'nullable|unique:users,phone,' . $this->segment(3),
                            'email'    => 'nullable|email|unique:users,email,' . $this->segment(3),
                            'password' => 'nullable|min:6|confirmed',
                            'image' => 'nullable|image',
                        ];
                    }
                }
            default:
                break;
        }
    }
}
