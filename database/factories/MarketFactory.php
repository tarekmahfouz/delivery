<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Market;
use Faker\Generator as Faker;

$factory->define(Market::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'lat' => (mt_rand(0, 1.0 * pow(10, 7)) / pow(10, 7)) + 30.0,
        'lon' => (mt_rand(0, 1.0 * pow(10, 7)) / pow(10, 7)) + 31.0,
    ];
});
