<?php

namespace App\Http\Services;

/**
 *  Tarek Mahfouz
 */

class CommonService
{
    private $model;
    private static $namespace = 'App\Models\\';
    
    public function find($model ,$conditions = [], $with = [])
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $item = count($with) ? $this->model->with($with) : $this->model;
        $item = $item->where($conditions)->first();

        return $item;
    }
    

    public function create($model ,array $data)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $item = $this->model->create($data);
        return $item;
    }

    public function bulkInsert($model ,array $data)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $items = $this->model->insert($data);
        return $items ? 'OK' : 'Error';
    }

    public function update($model ,$condition, $data)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $this->model->where($condition)->update($data);
        $item = $this->model->where($condition)->first();
        return $item;
    }

    public function destroy($model ,$condition = [], $whereIn = [])
    {
        $class = self::$namespace.$model;
        $this->model = new $class();
        $items = $this->model;
        if((count($whereIn) && isset($whereIn['key']) && isset($whereIn['values'])) && !count($condition))
            $items = $this->model->whereIn($whereIn['key'], $whereIn['values']);
        elseif(!count($whereIn) && count($condition))
            $items = $this->model->where($condition);
        elseif((count($whereIn) && isset($whereIn['key']) && isset($whereIn['values'])) && count($condition))
            $items = $this->model->where($condition)->whereIn($whereIn['key'], $whereIn['values']);
        //$this->model->where($condition)->whereIn($key, $whereIn)->delete();
        $items = $items->get();
        
        foreach($items as $item) {
            $item->delete();
        }
        return true;
    }


}
